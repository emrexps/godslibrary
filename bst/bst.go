package main

import (
	"fmt"
)

type BinaryNode struct {
	value int
	left  *BinaryNode
	right *BinaryNode
}

type BinarySearchTree struct {
	root *BinaryNode
}

func (bst *BinarySearchTree) insert(currentNode *BinaryNode, value int) *BinaryNode {
	if currentNode == nil {
		newNode := &BinaryNode{value: value}
		return newNode
	} else if value <= currentNode.value {
		currentNode.left = bst.insert(currentNode.left, value)
		return currentNode
	} else {
		currentNode.right = bst.insert(currentNode.right, value)
		return currentNode
	}
}

func (bst *BinarySearchTree) Insert(value int) {
	bst.root = bst.insert(bst.root, value)
}

func preOrder(node *BinaryNode) {
	if node == nil {
		return
	}
	fmt.Print(node.value, " ")
	preOrder(node.left)
	preOrder(node.right)
}

func inOrder(node *BinaryNode) {
	if node == nil {
		return
	}
	inOrder(node.left)
	fmt.Print(node.value, " ")
	inOrder(node.right)
}

func postOrder(node *BinaryNode) {
	if node == nil {
		return
	}
	postOrder(node.left)
	postOrder(node.right)
	fmt.Print(node.value, " ")
}

func (bst *BinarySearchTree) LevelOrder() {
	if bst.root == nil {
		return
	}
	queue := []*BinaryNode{bst.root}
	for len(queue) > 0 {
		presentNode := queue[0]
		queue = queue[1:]

		fmt.Print(presentNode.value, " ")

		if presentNode.left != nil {
			queue = append(queue, presentNode.left)
		}
		if presentNode.right != nil {
			queue = append(queue, presentNode.right)
		}
	}
}

func (bst *BinarySearchTree) Search(node *BinaryNode, value int) *BinaryNode {
	if node == nil {
		fmt.Println("Value:", value, "not found in BST")
		return nil
	} else if node.value == value {
		fmt.Println("Value:", value, "found in BST")
		return node
	} else if value < node.value {
		return bst.Search(node.left, value)
	} else {
		return bst.Search(node.right, value)
	}
}

func minimumNode(root *BinaryNode) *BinaryNode {
	if root.left == nil {
		return root
	}
	return minimumNode(root.left)
}

func (bst *BinarySearchTree) deleteNode(root *BinaryNode, value int) *BinaryNode {
	if root == nil {
		fmt.Println("Value not found in BST")
		return nil
	}
	if value < root.value {
		root.left = bst.deleteNode(root.left, value)
	} else if value > root.value {
		root.right = bst.deleteNode(root.right, value)
	} else {
		if root.left != nil && root.right != nil {
			temp := root
			minNodeForRight := minimumNode(temp.right)
			root.value = minNodeForRight.value
			root.right = bst.deleteNode(root.right, minNodeForRight.value)
		} else if root.left != nil {
			root = root.left
		} else if root.right != nil {
			root = root.right
		} else {
			root = nil
		}
	}

	return root
}

func (bst *BinarySearchTree) DeleteNode(value int) {
	bst.root = bst.deleteNode(bst.root, value)
}

func main() {
	bst := BinarySearchTree{}

	bst.Insert(10)
	bst.Insert(5)
	bst.Insert(15)
	bst.Insert(3)
	bst.Insert(7)
	bst.Insert(12)
	bst.Insert(18)

	fmt.Println("PreOrder Traversal:")
	preOrder(bst.root)
	fmt.Println()

	fmt.Println("InOrder Traversal:")
	inOrder(bst.root)
	fmt.Println()

	fmt.Println("PostOrder Traversal:")
	postOrder(bst.root)
	fmt.Println()

	fmt.Println("Level Order Traversal:")
	bst.LevelOrder()
	fmt.Println()

	searchedNode := bst.Search(bst.root, 12)
	if searchedNode != nil {
		fmt.Println("Searched Node:", searchedNode.value)
	}

	bst.DeleteNode(15)

	fmt.Println("InOrder Traversal after deletion:")
	inOrder(bst.root)
	fmt.Println()

}
